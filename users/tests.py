from django.test import TestCase

from .models import *


class UserTests(TestCase):
    def test_user_creation(self):
        user = User()
        self.assertIsInstance(user, User)

    def test_user_username(self):
        user = User(username='test_user')
        self.assertEqual(user.get_username(), 'test_user')